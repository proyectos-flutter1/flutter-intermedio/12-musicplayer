# Music Player App

Este proyecto es del curso de Fernando Herrera

Si quieres inscribirte en el curso, este es el enlace:
[Flutter: Diseños profesionales](https://fernando-herrera.com/#/curso/flutter-disenos)


![img](https://fernando-herrera.com/github/flutter/goal.png)

# Version de Flutter

Flutter 1.22.4 • channel stable • https://github.com/flutter/flutter.git
Framework • revision 1aafb3a8b9 (6 months ago) • 2020-11-13 09:59:28 -0800
Engine • revision 2c956a31c0
Tools • Dart 2.10.4